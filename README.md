# forex

A local proxy for getting Currency Exchange Rates

## Quickstart

Build a docker image and start service:

```shell
$ sbt docker:publishLocal
$ docker run -d -p 8888:8888 forex:1.0.0
$ curl -X GET "http://$(docker-machine ip):8888?from=usd&to=eur"
```

## API

| Endpoint| Method | Description                                                                     | 
|---------------------------------|-----|------------------------------------------------------------| 
| /?from=`<string>`&to=`<string>` | GET | Returns an exchange rate between `from` and `to` currencies| 

Returns:

```json
{
    "from": "USD",
    "to": "EUR",
    "price": 0.803936,
    "timestamp": "2018-02-04T23:23:47Z"
}
```

## Local run and tests

To run a service with sbt:

```shell
$ sbt run
```

To run integration tests:

```shell
$ sbt it:test
```

## Benchmark

```shell
$ wrk -t12 -c100 -d30s "http://$(docker-machine ip):8888/?from=usd&to=eur"
```

## About

This service is built with using of `Eff` monad.

### 1Forge client

1Forge service is built with `akka-http` as it is already 
listed in dependencies and wrapped into the `Live` interpreter.
Call to 1Forge, that returns a `Future`, is wrapped into a monix `Task`
with deferred execution.

### Cache

The `Memoization` effect is used for returning a rate that is 
not older than 5 minutes. In the `org.atnos.eff.Cache` two 
implementations miss an ability to set TTL. For that 
`_CaffeineCache` is implemented in the `forex.services.kv`. 
Parameters for instantiating a cache can be passed with the 
config in `reference.conf`.

### General notes

1. In case of 1Forge unavailability, the app does not return an 
exchange rate older than 5 minutes. Each incoming request will 
cause outgoing calls to 1Forge until success. This behaviour 
caused by safety reasons and should be later resolved by using
alternate data providers.

2. The service returns `json` entities.

3. Service does not include security and CORS headers. 
This behaviour should be changed in next releases.
