package forex.processes.rates

import forex.services._

object converters {
  import messages._

  def toProcessError[T <: Throwable](t: T): Error = t match {
    case e: OneForgeError.Generic.type ⇒ Error.Generic(e.service)
    case e: OneForgeError.Specific     ⇒ Error.Specific(e.service, e.getMessage)
    case e: OneForgeError.System       ⇒ Error.System(e.service, e.underlying)
    case e: Error                      ⇒ Error.Generic(e.service)
    case e                             ⇒ Error.System("unknown", e)
  }
}
