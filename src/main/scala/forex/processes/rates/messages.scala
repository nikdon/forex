package forex.processes.rates

import forex.domain._

import scala.util.control.NoStackTrace

package messages {

  sealed trait Error extends Throwable with NoStackTrace {
    def service: String
  }
  object Error {
    final case class Generic(service: String) extends Error {
      override val getMessage: String = "Something went wrong in the rates process"
    }
    final case class System(service: String, underlying: Throwable) extends Error
    final case class Specific(service: String, message: String) extends Error {
      override def getMessage: String = message
    }
  }

  final case class GetRequest(
      from: Currency,
      to: Currency
  )
}
