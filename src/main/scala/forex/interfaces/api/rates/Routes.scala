package forex.interfaces.api.rates

import akka.http.scaladsl._
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import forex.config._
import forex.interfaces.api.utils._
import forex.main._
import monix.cats._
import org.atnos.eff.Eff
import org.atnos.eff.addon.monix.task._
import org.zalando.grafter.macros._

@readerOf[ApplicationConfig]
case class Routes(
    processes: Processes,
    runners: Runners
) extends LazyLogging {
  import ApiMarshallers._
  import Converters._
  import Directives._
  import processes._
  import server.Directives._

  lazy val route: server.Route =
    get {
      getApiRequest { req ⇒
        complete {
          val app: Eff[AppStack, Protocol.GetApiResponse] =
            taskMemoized(
              req.show,
              Rates
                .get(toGetRequest(req))
                .flatMap {
                  case Left(err)   ⇒ taskFailed(err)
                  case Right(rate) ⇒ taskDelay(toGetApiResponse(rate))
                }
            )

          runners.runApp(app)
        }
      }
    }

}
