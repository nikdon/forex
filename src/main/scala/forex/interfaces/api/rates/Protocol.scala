package forex.interfaces.api.rates

import cats.Show
import forex.domain._
import io.circe._
import io.circe.generic.semiauto._

object Protocol {

  final case class GetApiRequest(
      from: Currency,
      to: Currency
  )

  object GetApiRequest {
    implicit val show: Show[GetApiRequest] = Show.show(a ⇒ s"${a.from}${a.to}")
  }

  final case class GetApiResponse(
      from: Currency,
      to: Currency,
      price: Price,
      timestamp: Timestamp
  )

  object GetApiResponse {
    implicit val encoder: Encoder[GetApiResponse] = deriveEncoder[GetApiResponse]
    implicit val decoder: Decoder[GetApiResponse] = deriveDecoder[GetApiResponse]
  }

}
