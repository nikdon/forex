package forex.interfaces.api.utils

import akka.http.scaladsl._
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpResponse }
import forex.interfaces.api.utils.ApiExceptionHandler.RejectionResponse
import io.circe.syntax._

object ApiRejectionHandler {

  def apply(): server.RejectionHandler =
    server.RejectionHandler.default
      .mapRejectionResponse {
        case res @ HttpResponse(_, _, ent: HttpEntity.Strict, _) ⇒
          val message = ent.data.utf8String.replaceAll("\"", """\"""")

          res.copy(
            entity = HttpEntity(
              ContentTypes.`application/json`,
              RejectionResponse(error = true, message = s"$message", "http").asJson.noSpaces
            )
          )

        case x ⇒ x
      }

}
