package forex.interfaces.api.utils

import akka.http.scaladsl._
import akka.http.scaladsl.model.StatusCodes
import akka.pattern.CircuitBreakerOpenException
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import forex.processes.RatesError
import io.circe.generic.JsonCodec
import io.circe.generic.extras.auto._

object ApiExceptionHandler extends FailFastCirceSupport {

  @JsonCodec final case class RejectionResponse(error: Boolean, message: String, service: String)

  def apply(): server.ExceptionHandler =
    server.ExceptionHandler {
      case err: RatesError ⇒
        ctx ⇒
          ctx.complete((StatusCodes.InternalServerError, RejectionResponse(error = true, err.getMessage, err.service)))

      case _: CircuitBreakerOpenException ⇒
        ctx ⇒
          ctx.complete(
            (
              StatusCodes.GatewayTimeout,
              RejectionResponse(error = true, "A timely response from an upstream server was not received", "upstream")
            )
          )

      case err: Throwable ⇒
        ctx ⇒
          ctx.complete((StatusCodes.InternalServerError, RejectionResponse(error = true, err.getMessage, "system")))
    }

}
