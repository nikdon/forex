package forex.main

import forex.config._
import forex.{ processes ⇒ p, services ⇒ s }
import monix.cats._
import org.zalando.grafter.macros._

@readerOf[ApplicationConfig]
case class Processes(oneForge: OneForgeConfig, actorSystems: ActorSystems, executors: Executors) {
  implicit lazy val as: ActorSystems = actorSystems
  implicit lazy val ec: Executors = executors

  implicit lazy val _oneForge: s.OneForge[AppEffect] = s.OneForge.live[AppStack](oneForge)
//  implicit lazy val _oneForge: s.OneForge[AppEffect] = s.OneForge.dummy[AppStack]

  final val Rates = p.Rates[AppEffect]
}
