package forex.main

import forex.config._
import forex.services.kv.Instances
import monix.cats._
import monix.eval.Task
import org.atnos.eff.syntax.addon.monix.task._
import org.atnos.eff.{ Cache, Eff }
import org.zalando.grafter.macros._

@readerOf[ApplicationConfig]
case class Runners(cacheConfig: CacheConfig) {

  lazy val C: Cache = Instances.caffeine(cacheConfig)

  def runApp[R](
      app: Eff[AppStack, R]
  ): Task[R] =
    app.runTaskMemo(C).runAsync
}
