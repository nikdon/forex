package forex.config

import org.zalando.grafter.macros._

import scala.concurrent.duration.FiniteDuration

@readers
case class ApplicationConfig(
    akka: AkkaConfig,
    api: ApiConfig,
    executors: ExecutorsConfig,
    cache: CacheConfig,
    oneForge: OneForgeConfig
)

case class AkkaConfig(
    name: String,
    exitJvmTimeout: Option[FiniteDuration]
)

case class ApiConfig(
    interface: String,
    port: Int
)

case class ExecutorsConfig(
    default: String,
    httpClient: String
)

case class CacheConfig(
    maximumSize: Int,
    expireAfterWrite: FiniteDuration
)

case class OneForgeConfig(
    baseUrl: String,
    version: String,
    apiKey: String,
    circuitBreaker: CircuitBreakerConfig
)

case class CircuitBreakerConfig(
    maxFailures: Int,
    callTimeout: FiniteDuration,
    resetTimeout: FiniteDuration,
    maxResetTimeout: FiniteDuration
)
