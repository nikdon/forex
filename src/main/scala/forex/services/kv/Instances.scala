package forex.services.kv

import java.util.concurrent.TimeUnit

import cats.Id
import cats.implicits._
import com.github.benmanes.caffeine.cache.{ Cache, Caffeine }
import forex.config.CacheConfig

import scalacache.Entry
import scalacache.caffeine.CaffeineCache
import scalacache.modes.sync._

object Instances {
  def caffeine(config: CacheConfig): org.atnos.eff.Cache = new _CaffeineCache(config: CacheConfig)
}

private[kv] final class _CaffeineCache(config: CacheConfig) extends org.atnos.eff.Cache {
  private[this] val underlying: Cache[String, Entry[Any]] =
    Caffeine
      .newBuilder()
      .expireAfterWrite(config.expireAfterWrite.toNanos, TimeUnit.NANOSECONDS)
      .maximumSize(config.maximumSize.toLong)
      .build[String, Entry[Any]]

  private[this] val scalaCache: CaffeineCache[Any] = CaffeineCache(underlying)

  override type C = org.atnos.eff.Cache

  override def memo[V](key: AnyRef, value: ⇒ V): V =
    get[V](key).fold(put[V](key, value))(identity)

  override def put[V](key: AnyRef, value: V): V =
    cats.Functor[Id].map(scalaCache.put[Id](key)(value, ttl = None))(_ ⇒ value).extract

  override def get[V](key: AnyRef): Option[V] =
    scalaCache.get[Id](key).extract.map(_.asInstanceOf[V])

  override def reset(key: AnyRef): C = {
    scalaCache.remove(key)
    this
  }
}
