package forex.services.oneforge

import scala.util.control.NoStackTrace

sealed trait Error extends Throwable with NoStackTrace {
  val service = "1Forge"
}
object Error {
  final case object Generic extends Error
  final case class Specific(text: String) extends Error {
    override def getMessage: String = text
  }
  final case class System(underlying: Throwable) extends Error
}
