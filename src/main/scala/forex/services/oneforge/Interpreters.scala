package forex.services.oneforge

import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.Uri
import akka.pattern.CircuitBreaker
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import forex.config.OneForgeConfig
import forex.domain._
import forex.domain.oneforge.OneForgeResponse
import forex.main.{ ActorSystems, Executors }
import monix.eval.Task
import org.atnos.eff._
import org.atnos.eff.addon.monix.task._

import scala.concurrent.{ ExecutionContext, Future }

object Interpreters {
  def dummy[R: _task](): Algebra[Eff[R, ?]] = new Dummy[R]

  def live[R: _task](oneForge: OneForgeConfig)(
      implicit
      actorSystems: ActorSystems,
      executors: Executors
  ): Algebra[Eff[R, ?]] = new Live[R](oneForge)
}

final class Dummy[R: _task] private[oneforge] extends Algebra[Eff[R, ?]] {
  override def get(
      pair: Rate.Pair
  ): Eff[R, Error Either Rate] =
    for {
      result ← fromTask(Task.delay(Rate(pair, Price(BigDecimal(100)), Timestamp.now)))
    } yield Right(result)
}

private[oneforge] final class Live[R: _task](oneForge: OneForgeConfig)(
    implicit
    actorSystems: ActorSystems,
    executors: Executors,
) extends Algebra[Eff[R, ?]]
    with ErrorAccumulatingCirceSupport
    with LazyLogging {

  lazy final val CONVERT_URL = s"${oneForge.baseUrl}/${oneForge.version}/convert"

  import actorSystems._

  implicit val ec: ExecutionContext = executors.httpClient
  lazy val http = Http()

  lazy val breaker: CircuitBreaker = {
    CircuitBreaker(
      scheduler = system.scheduler,
      maxFailures = oneForge.circuitBreaker.maxFailures,
      resetTimeout = oneForge.circuitBreaker.resetTimeout,
      callTimeout = oneForge.circuitBreaker.callTimeout
    ).withExponentialBackoff(oneForge.circuitBreaker.maxResetTimeout)
      .onOpen(logger.warn(s"CircuitBreaker is open after ${oneForge.circuitBreaker.maxFailures} failed calls"))
      .onClose(logger.info("CircuitBreaker is closed after the successful call"))
      .onHalfOpen(logger.info(s"CircuitBreaker is half open after"))
  }

  override def get(pair: Rate.Pair): Eff[R, Either[Error, Rate]] = {
    def responseF: Future[OneForgeResponse] = {
      val params = List(
        ("from", s"${pair.from}"),
        ("to", s"${pair.to}"),
        ("quantity", "1"),
        ("api_key", oneForge.apiKey)
      )

      val request = RequestBuilding.Get(Uri(CONVERT_URL).withQuery(Uri.Query(params: _*)))

      for {
        response ← breaker.withCircuitBreaker(http.singleRequest(request))
        unmarshalled ← unmarshaller[OneForgeResponse](OneForgeResponse.oneForgeResponseDecoder)
          .apply(response.entity)
          .recoverWith {
            case err ⇒ response.entity.discardBytes().future().flatMap(_ ⇒ Future.failed(err))
          }
      } yield unmarshalled
    }

    def decodedF: Future[Either[Error, Rate]] = responseF.map {
      case ofr: oneforge.OneForgeResponse.Conversion ⇒
        Right(Rate(pair, Price(ofr.value), ofr.timestamp))

      case err: oneforge.OneForgeResponse.Error ⇒
        Left(Error.Specific(err.message))
    }

    fromTask[R, Either[Error, Rate]](Task.deferFuture(decodedF))
  }
}
