package forex.domain.oneforge

import forex.domain.Timestamp
import io.circe.Decoder
import io.circe.generic.JsonCodec

@JsonCodec sealed trait OneForgeResponse extends Product with Serializable

object OneForgeResponse {
  @JsonCodec final case class Error(error: Boolean, message: String) extends OneForgeResponse
  @JsonCodec final case class Conversion(value: BigDecimal, text: String, timestamp: Timestamp) extends OneForgeResponse

  lazy val oneForgeResponseDecoder: Decoder[OneForgeResponse] = {
    def widen[A: Decoder, AA <: A: Decoder]: Decoder[A] =
      implicitly[Decoder[AA]].map[A](identity)

    widen[OneForgeResponse, Conversion] or widen[OneForgeResponse, Error]
  }
}
