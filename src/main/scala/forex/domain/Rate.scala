package forex.domain

import cats.Show
import io.circe._
import io.circe.generic.semiauto._

case class Rate(
    pair: Rate.Pair,
    price: Price,
    timestamp: Timestamp
)

object Rate {
  final case class Pair(
      from: Currency,
      to: Currency
  )

  object Pair {
    implicit val encoder: Encoder[Pair] =
      deriveEncoder[Pair]

    implicit val show: Show[Pair] = Show.fromToString
  }

  implicit val encoder: Encoder[Rate] =
    deriveEncoder[Rate]
}
