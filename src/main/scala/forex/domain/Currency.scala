package forex.domain

import cats.Show
import enumeratum._

import scala.collection.immutable

sealed trait Currency extends Product with Serializable with EnumEntry
object Currency extends Enum[Currency] with CirceEnum[Currency] {
  val values: immutable.IndexedSeq[Currency] = findValues

  final case object AUD extends Currency
  final case object CAD extends Currency
  final case object CHF extends Currency
  final case object EUR extends Currency
  final case object GBP extends Currency
  final case object NZD extends Currency
  final case object JPY extends Currency
  final case object SGD extends Currency
  final case object USD extends Currency

  implicit val show: Show[Currency] = Show.fromToString
}
