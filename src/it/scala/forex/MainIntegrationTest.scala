package forex

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.{ RouteTestTimeout, ScalatestRouteTest }
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import forex.interfaces.api.rates.Protocol.GetApiResponse
import forex.interfaces.api.utils.ApiExceptionHandler.RejectionResponse
import forex.main.Application
import monix.eval.Task
import monix.execution.Scheduler
import org.scalatest.{ BeforeAndAfterAll, FlatSpec, Matchers }

import scala.concurrent.Await
import scala.concurrent.duration._

class MainIntegrationTest
    extends FlatSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll
    with ErrorAccumulatingCirceSupport {

  behavior of "Main App"
  val main: Main.type = Main
  var app: Application = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    implicit lazy val scheduler: Scheduler = Scheduler.apply(system.dispatcher)
    val start = Task.now(main.main(Array.empty))
    val await = Task.unit.delayExecution(10.seconds)
    val startup = for {
      _ ← start
      _ ← await
    } yield main.app.fold(sys.exit(1))(a ⇒ app = a)
    Await.ready(startup.runAsync, 15.seconds)
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    app.api.actorSystems.system.terminate()
  }

  it should "return an error response in case of empty params" in {
    val routes = app.api.routes.route

    Get() ~> routes ~> check {
      response.status should not be StatusCodes.OK
    }
  }

  it should "return an exchange rate for EUR-USD pair" in {
    import akka.testkit._
    implicit val timeout: RouteTestTimeout = RouteTestTimeout(10.seconds.dilated)

    val routes = app.api.routes.route

    Get("?from=usd&to=eur") ~> routes ~> check {
      response.status shouldBe StatusCodes.OK
      responseAs[GetApiResponse] shouldBe a[GetApiResponse]
    }
  }

  it should "return a rejection with 'malformed' in case of wrong params" in {
    val routes = app.api.routes.route

    Get("?from=ASTROCOINS&to=EUR") ~> routes ~> check {
      response.status shouldBe StatusCodes.BadRequest
      val res = responseAs[RejectionResponse]
      res.error shouldBe true
      res.message should include("malformed")
    }
  }

  it should "return an exchange rate from cache in case of duplicate request" in {
    val routes = app.api.routes.route

    Get("?from=usd&to=eur") ~> routes ~> check {
      val a = responseAs[GetApiResponse]
      Get("?from=usd&to=eur") ~> routes ~> check {
        val b = responseAs[GetApiResponse]

        a shouldBe b
      }
    }
  }
}
